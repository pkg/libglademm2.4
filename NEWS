2.6.7:

* Windows build fixes.
  (Armin Burgmeier)

2.6.6:

* Xml::connect_clicked((): Handle ToolButtons too.
  (Dave Moore. Bug #439339)

2.6.5:

* Fix the build when glibmm was built with exceptions disabled.
  (Armin Burgmeier)

2.6.4:

* Xml(const char* buffer, ) constructor: Make this actually 
  work. (Douglas C. MacKenzie) Bug #326512.
* Documentation: Clarified lifetime rules. (Ole Laursen)
* Build:
  - Fixed build when disabling properties and exceptions.
  (Armin Burgmeier, Openismus)
  - Check for both m4 and M4 in the GNU m4 output, to fix 
  the build on some platforms. (Yselkowitz) Bug #42399.

2.6.3:

* Build fixes for Updated for Visual Studio 2005.
(Cedric Gustin)

2.6.2:

* Glade::get_widget(): Show the name of not-found 
  widgets on the console, because libglade does not seem to 
  do this anymore (I think it did once.)
* Example: Delete the dialog when we are finished with it.
  (Murray Cumming)
* Windows build fixes. (Cedric Gustin)

2.6.1:

* Windows build fixes. (Cedric Gustin)

2.6.0:

libglademm 2.6 is API/ABI-compatible with libglademm 2.4.
  
Changes since libglademm 2.4:

* Glade::Xml: Added connect_clicked() for easy connection of
  buttons and menuitems signals. (Alberto Paro)
* Glade::VariablesMap: now handles Entry, ComboTextEntry, 
  SpinButton, *Scale, Calendar and CheckBox. (Alberto Paro).


2.5.1:

* Glade::Xml: Added connect_clicked() for easy connection of
  buttons and menuitems signals. (Alberto Paro)
* Glade::VariablesMap: now handles Entry, ComboTextEntry, 
  SpinButton, *Scale, Calendar and CheckBox. (Alberto Paro).

2.4.1:

* gcc 3.4 build fix. (Bryan Forbes)

2.4.0:

The API is now stable.

2.3.3:

* get_widget():
  - Don't create a C++ object with a null C object, when the 
  C object can not be found. (jdhall)
  - Really return existing widget wrappers. (Murray Cumming)
* Stop accidental need for doxygen when building from tarballs.
  (Murray Cumming)
* win32: Test for -mms-bitfields / -fnative-struct.

2.3.2:
 
* Updated for new libsigc++ 2 syntax.

2.3.1:

Documentation:
- Generate and install devhelp file.
- beautify hmtl documentation, like the gtkmm docs.
- Install html documentation.

2.3.0:
 
This is the new unstable branch of the parallel-installable libglademm 2.4,
for use with gtkmm 2.4.

2.1.2:

* get_widget_derived() now returns an existing C++ instance if one exists, like
  get_widget() already does. (Murray Cumming)

2.1.1:

No changes. Just recreated the tarball - it was not installing the .la files.

2.1.0:

This is the development phase for libglademm 2.2.0,
which will add API but will not break API or ABI.

* Added Glade::Xml::get_widget_derived() and 
  examples/derived/ to demonstrate it.
  This allows you to use Glade to layout your
  own derived container widgets. Tell me what you
  think.
  (Murray Cumming)


2.0.1:

* Rebuilt the tarball with newer autotools, fixing the $SED problem.

2.0.0

* Glade::Xml::create() now throws an XmlError expection if it fails.
  (Daniel Elstner)
* Minimise use of templates, to reduce application code size.
  (Daniel Elstner)

1.3.5




